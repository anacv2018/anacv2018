﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace studenti
{
    public class student
    {
        public string ime { get; set; }
        public string prezime { get; set; }
        public int starost { get; set; }
        public List<int> ocene = new List<int>();
        public List<student> studenti;

        public student()
        {
        }

        public List<student> getstudenti()
        {
            this.studenti = new List<student>
            {
                new student
                {
                    ime = "marko",
                    prezime = "kraljevic",
                    starost = 17,
                    ocene = {2, 3, 4, 8, 9}
                },
                new student
                {
                    ime = "vuk",
                    prezime = "karadzic",
                    starost = 20,
                    ocene = {3, 4, 5, 6, 7}
                },
                new student
                {
                    ime = "filip",
                    prezime = "visnjic",
                    starost = 21,
                    ocene = {4, 5, 6, 8}
                },
                new student
                {
                    ime = "djordje",
                    prezime = "petrovic karadjordje",
                    starost = 25,
                    ocene = {1, 5, 8}
                },
                new student
                {
                    ime = "petar",
                    prezime = "petrovic njegos",
                    starost = 33,
                    ocene = {4, 6, 6}
                }
            };

            return this.studenti;
        }
    }
}
